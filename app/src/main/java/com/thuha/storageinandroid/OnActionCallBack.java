package com.thuha.storageinandroid;

public interface OnActionCallBack {
    void onCallBack(String key, Object obj);
}
