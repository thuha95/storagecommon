package com.thuha.storageinandroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Frg1 extends BaseFragment implements View.OnClickListener {
    public static final String KEY_SUM = "KEY_SUM ";
    public static final String KEY_SUB = "KEY_SUB ";
    public static final String TAG = Frg1.class.getName();
    private EditText numA, numB;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_1, container, false);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View rootView) {
        rootView.findViewById(R.id.bt_sum).setOnClickListener(this);
        rootView.findViewById(R.id.bt_sub).setOnClickListener(this);

        numA = rootView.findViewById(R.id.edt_soA);
        numB = rootView.findViewById(R.id.edt_soB);
        initData();


    }

    private void initData() {
        SharedPreferences pref = App.getInstance().getSharedPreferences(MainActivity.SHARE_FILE, Context.MODE_PRIVATE);
        String txtA = pref.getString("key_a", null);
        String txtB = pref.getString("key_b", null);
        getStorage().setmF1Data(new String[]{txtA, txtB});
        if (getStorage().getmF1Data() != null) {
            numA.setText(getStorage().getmF1Data()[0]);
            numB.setText(getStorage().getmF1Data()[1]);
        }
    }

    @Override
    public void onClick(View v) {
        String a = numA.getText().toString();
        String b = numB.getText().toString();
        getStorage().setmF1Data((new String[]{a, b}));
        switch (v.getId()) {
            case R.id.bt_sum:
                mCallBack.onCallBack(KEY_SUM, new String[]{a, b});
                break;
            case R.id.bt_sub:
                mCallBack.onCallBack(KEY_SUB, new String[]{a, b});
                break;
            default:
                break;
        }
    }
}
