package com.thuha.storageinandroid;

import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class CommonUtil {
    private SharedPreferences pref;

    public CommonUtil() {
        pref = App.getInstance().getSharedPreferences(MainActivity.SHARE_FILE, MODE_PRIVATE);
    }

    public void savePref(String key, String value, boolean isOverride) {
        if (isOverride) {
            pref.edit().putString(key, value);
        } else {
            pref.edit().clear();
            pref.edit().putString(key, value);

        }

    }

    public String getPref(String key) {
        return pref.getString(key, null);
    }

    public void clearPref(String key) {
        pref.edit().remove(key);
    }


}
