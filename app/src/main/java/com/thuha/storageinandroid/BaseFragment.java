package com.thuha.storageinandroid;

import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {
    protected OnActionCallBack mCallBack;

    public void setCallBack(OnActionCallBack callBack) {
        mCallBack = callBack;
    }

    protected StorageCommon getStorage(){
        return App.getInstance().getStorage();
    }

}
