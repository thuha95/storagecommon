package com.thuha.storageinandroid;

public final class StorageCommon {
    private String[] mF1Data;

    public void setmF1Data(String[] num) {
        this.mF1Data = num;
    }

    public String[] getmF1Data() {
        return mF1Data;
    }

    public void clearData() {
        mF1Data = null;
    }


    public StorageCommon() {
        clearData();
    }
}
