package com.thuha.storageinandroid;

import android.app.Application;

public class App extends Application {
    private static App instance;
    private StorageCommon storage;


    //phuong thuc onCreate chay 1 lan duy nhat khi ung dung duoc khoi tao
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        storage = new StorageCommon();
    }

    public StorageCommon getStorage() {
        return storage;
    }

    public static App getInstance() {
        return instance;
    }
}
