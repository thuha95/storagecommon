package com.thuha.storageinandroid;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.lang.reflect.Constructor;

public class MainActivity extends AppCompatActivity implements OnActionCallBack {

    private static final String[] PERMISSTION = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private SharedPreferences pref;
    public static final String SHARE_FILE = "SHARE_FILE.pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        for (String permission : PERMISSTION) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, PERMISSTION, 1);
            }
        }
        initViews();
    }

    private void initViews() {
        showFrg(Frg2.TAG);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int i = 0; i < grantResults.length; i++) {
            if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                finish();
                return;
            }
        }
        initViews();
    }

    public void showFrg(String tag) {
        try {
            Class<?> clazz = null;
            clazz = Class.forName(tag);
            Constructor<?> constructor = clazz.getConstructor();
            BaseFragment frg = (BaseFragment) constructor.newInstance();
            frg.setCallBack(this);
            getSupportFragmentManager().beginTransaction().replace(R.id.ln_main, frg, tag).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCallBack(String key, Object obj) {
        String[] num = (String[]) obj;
        double a = Double.parseDouble(num[0]);
        double b = Double.parseDouble(num[1]);
        if (key.equals(Frg1.KEY_SUM)) {
            Toast.makeText(this, a + b + "", Toast.LENGTH_SHORT).show();
        } else if (key.equals(Frg1.KEY_SUB)) {
            Toast.makeText(this, a - b + "", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        saveData();
        super.onDestroy();
    }

    private void saveData() {
        pref = getSharedPreferences(SHARE_FILE, MODE_PRIVATE);
        pref.edit().putString("key_a", getStorage().getmF1Data()[0]).putString("key_b", getStorage().getmF1Data()[1]).apply();
    }

    private StorageCommon getStorage() {
        return App.getInstance().getStorage();
    }
}
