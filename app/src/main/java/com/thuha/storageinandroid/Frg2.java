package com.thuha.storageinandroid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class Frg2 extends BaseFragment implements View.OnClickListener {
    public static final String TAG = Frg2.class.getName();
    private ImageView img;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_2, container, false);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View rootView) {
        rootView.findViewById(R.id.bt_save).setOnClickListener(this);
        rootView.findViewById(R.id.bt_show).setOnClickListener(this);

        img = rootView.findViewById(R.id.img);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_save:
//                saveToSystemStorage();
                saveToStorage();
                break;
            case R.id.bt_show:
//                showImageFromSystemStorage();
                showImageFromStorage();
                break;
        }

    }

    private void saveToSystemStorage() {
        String savePath = Environment.getDataDirectory() + "/data/" + App.getInstance().getPackageName();
        saveFile(savePath);
    }

    private void saveToStorage() {
//        String savePath = Environment.getDataDirectory().getAbsolutePath();
        String savePath = App.getInstance().getExternalFilesDir(null).getPath();
        saveFile(savePath);
    }

    private void showImageFromStorage() {
        String savePath = App.getInstance().getExternalFilesDir(null).getPath();
        String fileName = "ic_animal_race.jpg";
        Bitmap bitmap = BitmapFactory.decodeFile(savePath + "/" + fileName);
        img.setImageBitmap(bitmap);
    }

    private void showImageFromSystemStorage() {
        String savePath = Environment.getDataDirectory() + "/data/" + App.getInstance().getPackageName();
        String fileName = "ic_animal_race.jpg";
        Bitmap bitmap = BitmapFactory.decodeFile(savePath + "/" + fileName);
        img.setImageBitmap(bitmap);
    }

    private void saveFile(String savePath) {
        try {
            InputStream in = App.getInstance().getAssets().open("ic_animal_race.jpg");
            String fileName = "ic_animal_race.jpg";

            File file = new File(savePath + "/" + fileName);
            FileOutputStream out = new FileOutputStream(file);

            byte[] buff = new byte[1024];
            int length = in.read(buff);
            while (length > 0) {
                out.write(buff, 0, length);
                length = in.read(buff);
            }

            out.close();
            in.close();
            Log.i("HaHA", "succes");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
